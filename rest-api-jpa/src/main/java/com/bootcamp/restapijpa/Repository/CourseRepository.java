package com.bootcamp.restapijpa.Repository;

import com.bootcamp.restapijpa.Entity.Course;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
