package com.bootcamp.restapijpa.Repository;

import com.bootcamp.restapijpa.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
